- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
- title: 'Manage your toolchain before it manages you'
  url: /resources/whitepaper-forrester-manage-your-toolchain/
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - Software development
    - DevOps
    - Toolchain
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Reduce toolchain complexity for improved software delivery and quality.'

- title: 'Reduce cycle time to deliver value'
  url: /resources/whitepaper-reduce-cycle-time/
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Learn how to stay competitive in a rapidly changing technological landscape.'

- title: 'Avoiding the DevOps tax'
  url: /webcast/avoid-devops-tax/
  image: /images/resources/devops.png
  type: 'Webcast'
  topics:
    - DevOps
    - Software development
  solutions:
    - Accelerated delivery
  teaser: 'Watch now to simplify your toolchain and realize a faster DevOps lifecycle.'
  
- title: 'Gartner Magic Quadrant for EAPT'
  url: /resources/report-gartner-magic-quadrant-eapt/
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
    - Agile delivery
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Download the Gartner Magic Quadrant for Enterprise Agile Planning Tools (EAPT).'

- title: 'Continuous Integration (CI) tools evaluation'
  url: /resources/forrester-wave-ci-2017/
  image: /images/resources/continuous-integration.png
  type: 'Report'
  topics:
    - CI
    - Software development
    - Git
    - GitLab
    - DevOps
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Read the Forrester Wave report, assessing the top Continuous Integration providers.'

- title: 'Planning for success'
  url: /resources/whitepaper-planning-for-success/
  image: /images/resources/security.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
    - Agile software development
  solutions:
    - Accelerated delivery
    - Distributed teams
    - Executive visibility
  teaser: 'Learn how to quickly iterate and ship code in the DevOps lifecycle.'

- title: 'Gartner Magic Quadrant for ARO'
  url: /resources/gartner-aro/
  image: /images/resources/security.png
  type: 'Report'
  topics:
    - CI
    - CD
    - Software development
    - Git
    - GitLab
    - DevOps
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Download the Gartner Magic Quadrant for Application Release Orchestration (ARO).'

- title: 'Speed to Mission: the strategic value of velocity'
  url: /resources/whitepaper-speed-to-mission/
  image: /images/resources/gitlab.png
  type: 'Report'
  topics:
    - DevOps
    - Software development
    - GitLab
    - Agile software development
    - Public sector
  solutions:
    - Accelerated delivery
  teaser: 'discover how to accelerate software innovation and meet your mission challenge.'

- title: 'A seismic shift in application security'
  url: /resources/whitepaper-seismic-shift-application-security/
  image: /images/resources/security.png
  type: 'Whitepaper'
  topics:
    - Security
    - DevSecOps
  solutions:
    - Security and quality
    - Distributed teams
  teaser: 'Learn how to integrate and automate security in the DevOps lifecycle.'

- title: 'Bridging the divide between developers and management'
  url: /resources/whitepaper-bridging-the-divide/
  image: /images/resources/gitlab.png
  type: 'Whitepaper'
  topics:
    - GitLab
    - Git
    - Software development
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Explore methods to align perceptions, expectations, and outcomes.'

- title: 'Concurrent DevOps'
  url: /resources/whitepaper-concurrent-devops/
  image: /images/resources/devops.png
  type: 'Whitepaper'
  topics:
    - GitLab
    - Software development
    - DevOps
  solutions:
    - Accelerated delivery
  teaser: 'Find out how Concurrent DevOps can help you fully realize the benefits of digital transformation.'

- title: 'Scaled continuous integration and delivery (CI/CD)'
  url: /resources/whitepaper-scaled-ci-cd/
  image: /images/resources/continuous-integration.png
  type: 'Whitepaper'
  topics:
    - CI
    - CD
    - DevOps
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Deliver what customers want, faster, with an integrated CI/CD tool.'

- title: 'Moving to Git'
  url: /resources/whitepaper-moving-to-git/
  image: /images/resources/git.png
  type: 'Whitepaper'
  topics:
    - Git
  solutions:
    - Accelerated delivery
    - Executive visibility
  teaser: 'Learn how to implement Git and take the first steps towards a more collaborative and efficient software development lifecycle.'

- title: 'What you need to know about going Cloud Native'
  url: /resources/video-going-cloud-native/
  image: /images/resources/cloud-native.png
  type: 'Webcast'
  topics:
    - Cloud native
  solutions:
    - Accelerated delivery
    - Distributed teams
  teaser: 'Find out how going Cloud-Native can make your business more flexible, reduce costs, increase collaboration, and improve security.'

- title: 2018 Global developer report
  url: /developer-survey/2018
  image: /images/resources/devops.png
  type: 'Report'
  topics:
    - Software development
    - DevOps
  teaser: 'Explore the findings of our global developer report from 2018.'

- title: 'GitLab capabilities statement'
  url: /images/press/gitlab-capabilities-statement.pdf
  image: /images/resources/gitlab.png
  type: 'One-pager'
  topics:
    - GitLab
    - Public sector
  solutions:
    - Distributed teams
    - Accelerated delivery
  teaser: 'Read the capabilities of GitLab as a single solution for DevOps.'

- title: 'GitLab datasheet'
  url: /images/press/gitlab-data-sheet.pdf
  image: /images/resources/gitlab.png
  type: 'One-pager'
  topics:
    - GitLab
    - DevOps
    - Single application
  solutions:
    - Project compliance
  teaser: 'Read how GitLab is a complete DevOps platform, delivered as a single application.'
