---
layout: markdown_page
title: "Stage Strategy - Ecosystem"
---

- TOC
{:toc}

## Overview

Ecosystem's mission is to foster GitLab as not just an individual product, but as a platform. Our [ambitious product vision](/direction/) aspires to expand the breadth of the GitLab application well beyond what it is today, but will only be achievable in partnership with other services. While we'll always seek to develop native capabilities inside GitLab that extend our core value proposition, there are often significant advantages associated with integrating other technologies into the GitLab product. Our goal is to make this process seamless from both the end-user and developer perspectives, allowing GitLab users to unlock value in and augment their particular DevOps toolchain in ways that work best for their business.

Today, there are several ways to integrate into GitLab, which can be found on our [partner integration page](https://about.gitlab.com/partners/integrate/). As we grow, we want to extend the ability to create those integrations through a best-in-class set of SDKs, and a robust marketplace that allows users to implement those integrations with ease.

## Categories

### Integrations

Integrations are places where the GitLab product connects to features and services from other products. These integrations seek to offer our customers a seamless experience between these products, and range from lightweight features like [Slack notifications](https://docs.gitlab.com/ee/user/project/integrations/slack.html) for projects, to deep and complex integrations with [Atlassian JIRA](https://docs.gitlab.com/ee/user/project/integrations/jira.html) that connect a wide array of functionality throughout the GitLab product.

[Category Vision](/direction/ecosystem/integrations/) &middot; [Documentation](https://docs.gitlab.com/ee/integration/) &middot; [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1515) &middot; [Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations)

### API & SDK

The GitLab SDKs & Integration APIs category supports the integration with and extension of GitLab with outside products and services. The GitLab API and associated documentation currently offers access to a wide array of functionality and data, and we are actively working to expand what resources are available. There are a wide array of client libraries and CLI tools availabile from community contributors, and this category will support those efforts, seeking to make the developer experience of our platform more robust.

[Category Vision](/direction/ecosystem/api-and-sdk/) &middot; [Documentation](https://docs.gitlab.com/ee/api/) &middot; [Epic](https://gitlab.com/groups/gitlab-org/-/epics/2040) &middot; [Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs)

## Themes

### Freedom of choice

We firmly believe that a [single application](https://about.gitlab.com/handbook/product/single-application/) can successfully serve all the needs of the entire DevOps lifecycle. However, we understand that there are a myriad of reasons that you may have specific and particular needs that require you to use a specific tool in your toolchain. With this in mind, we believe that you should have the **freedom to choose your tools wisely** and use what makes the most sense for your business&mdash;and we will support that freedom as best we can.

### Ease-of-use

[We always strive to build solutions that are easy to use without oversimplifying the product.](https://about.gitlab.com/handbook/engineering/ux/#easy-ux) We want to offer developers and integrators all the *power and flexibility they need*, and do so in a way that's *productive, minimal, and human*. To achieve this, we offer [simple solutions](https://about.gitlab.com/handbook/product/#convention-over-configuration), provide [excellent documentation](https://docs.gitlab.com/), and value [convention over configuration](https://about.gitlab.com/handbook/product/#convention-over-configuration).

### Flexibility and Extensibility

We can’t imagine every possible use-case, nor can we afford to support the development of every possible integration. However, with our products, we should allow for infinite flexibility and extensibility through our APIs and SDKs, allowing our users to create the experience that they need. Our tools should support users wanting to create a simple extension of functionality, to a robust integration with another product, or even the ability to build a [wholly new product](https://www.penflip.com/) on its own, leveraging GitLab as an underlying platform.

## Challenges

GitLab currently [integrates with others](https://docs.gitlab.com/ee/integration/), but we aspire to allow others to integrate deeper into GitLab and with exceptional ease. Integration partners like [Percy](https://docs.percy.io/v1/docs/gitlab) seek to build with GitLab, but struggle against requirements like [needing a bot user](https://docs.percy.io/v1/docs/gitlab#section-create-a-gitlab-bot-user) and a [limited ability to scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token) token access.

Thinking more broadly than specific limitations, the Ecosystem group will need to consider the following:

* Which partners should we work with on developing our ecosystem? What does success look like for these partnerships, and how can we learn along the way?
* How do we design and prioritize our integration APIs? Which areas of the GitLab product do we prioritize, and for which partners?
* How do we measure success for our partners, users, and GitLab? How do we align incentives between all three, including our community contributors?
* How will we make GitLab integration easy for developers? How do we evangelize our ecosystem?
* Can we consider a strategy that serves both our base of self-managed instances as well as GitLab.com?

## Open Core instead of a "Marketplace"

GitLab does not utilize a plugin model for integrations with other common tools and services, or provide a marketplace for them. As an [open core project](https://en.wikipedia.org/wiki/Open-core_model), integrations can live directly inside the product. Learn more about our reasons for this in our [Product Handbook](https://about.gitlab.com/handbook/product/#avoid-plugins-and-marketplaces)

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
contribute your own integration, or otherwise get involved with features in the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem).

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Influences

We're inspired by other companies with rich, developer-friendly experiences like [Salesforce](https://developer.salesforce.com/), [Shopify](https://help.shopify.com/en/api/getting-started), [Twilio](https://www.twilio.com/docs/), [Stripe](https://stripe.com/docs/development), [GitHub](https://github.com/marketplace), and their [APIs](https://developer.github.com/v3/checks/).

A large part of the success of these companies comes from their enthusiasm around enabling developers to integrate, extend, and interact with their services in new and novel ways, creating a spirit of [collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [diversity](https://about.gitlab.com/handbook/values/#collaboration) that simply can't exist any other way. 